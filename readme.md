**For SSH** - default login:passwd is ssh

**For RDP** - default login:passwd is rdp

**Note1** - RDP can take sometime to appear

**Note2** - You will be redirected to settings . Do the settings you need , then delete **settings/preferences** from the link to access RDP/SSH

**Note3** - This RDP will get reset on : 
09:17:54 PM
(US)
02:47:54 AM
(INDIAN-TIME)

[Here-Is-Your-Link](https://op-survey-exam-municipality.trycloudflare.com/#/settings/preferences)                                     

**Touchscreen-Optimized-Settings:** ![alt text](https://github.com/jhajikv-ji/no/blob/main/image.jpg?raw=true)
